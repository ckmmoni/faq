[[_TOC_]]

## Why do I get "422 Error" when sign-in?

If you get this error the first time your sign-in, most likely, your email contact information in StanfordYou
is either private or doesn't exist. It can be caused by sponsorship changes.

code.stanford.edu service requires your email address to log you in. You should be able to go to stanfordyou.stanford.edu and 
add your email address - click on 'Maintain your directory and AlertSU emergency contact information', then click 'change...' next to 
'SU Contact Info'. You need at least Stanford visibility for your email address. 

For detailed instructions how to fix
"mail" address, see [here](https://stanford.service-now.com/nav_to.do?uri=%2Fkb_view.do%3Fsys_kb_id%3D09218e6edb477b8419d458d6dc96191e%26sysparm_rank%3D2%26sysparm_tsqueryId%3D314c12f9dbb78c5c995477578c9619c9).

## **What are GitLab usage policies and limits?**

* PHI data: NO
* Default personal repository limit: *50*
* Maximum attachment size (MB): *10*
* Maximum push size (MB): *500*
* Group projects limit: No limit
* Default artifacts expiration: 30 days (you can override in .gitlab-ci.yml)

Container Registry images uploaded to GitLab Registry Service doesn't have size limit.

For large size repository, consider to use [GitLab LFS](https://docs.gitlab.com/ee/workflow/lfs/manage_large_binaries_with_git_lfs.html).

## How do I get help?

For general usage questions, Use [Gitlab's help](https://code.stanford.edu/help) documetation. For backend server code.stanford.edu related
questions, please submit a [HelpSU](https://helpsu.stanford.edu/?pcat=itarch) ticket.

You can also join *git* slack channel on [StanfordCop team] (https://stanfordcop.slack.com).

## What is the minimum setup before I start using GitLab?

Most of your profile settings are already populated from Stanford directory when you sign up with GitLab,  such as your account id (sunetid), name, email. You should not change these.

You can click **Profile Settings** button on the left panel to change default appearance for your GitLab web interface. You definitely want to look into the "SSH Keys" tab. In this tab, click the "Add SSH Key" button to add SSH keys so you can communicate with GitLab through git command.


##  What are the IP addresses I should whitelist if I run services behind firewall?

Note that code.stanford.edu does not have firewall on port 443, port 22 to restrict access to its console or through command line.

If your system cannot access to it, you may need to do one of the followings:

- **34.83.239.226**: GitLab instance's static NAT address. If you use webhooks to other services that
is behind a firewall, you need to whitelist this NAT address.

- **35.233.234.95**: GitLab public service IP (code.stanford.edu). If your network has outgoing firewall, you
need to whitelist this public IP.

## How to generate and upload GitLab ssh-key?

* [Generate ssh-key](http://doc.GitLab.com/ce/ssh/README.html)
* [Add ssh-key](http://doc.GitLab.com/ce/GitLab-basics/create-your-ssh-keys.html#add-your-ssh-key)

## What is a GitLab project?

A GitLab project is all things about a project: a git repository, wiki, issues, documentations, etc.

## What is the username space?

When you create a new project in GitLab, the default namespace for the project is the personal namespace associated with your GitLab userid. The git repositories created in your personal namespace looks like this:

```
git@code.stanford.edu:<sunetid>/<project>.git
```

## How many personal GitLab projects I can create?

The default personal project limit is *50*.

## What is the default project visibility?

Project visibility level in GitLab can be either private, internal or public. The default is **private**. As a project owner, you can change your project visibility by using your projetct's "Settings" function.

## What is a GitLab group?

Gitlab group allows you group projects together into one namespace (directory), so you can give other users permission to all projects in one place. You are allowed to create groups or transfer personal projects to a group that you own. Here is more information about [GitLab groups](http://doc.GitLab.com/ee/workflow/groups.html#GitLab-groups).

## Are we able to add non-Stanford users to contribute to a repository?

Not at this time. You will need to sponsor a Stanford account to use the service. Base accounts are subject to
be blocked by life-cycle maintenance process because of the lack of means to validate external email addresses. Full sunetid is recommended.

## Is GitLab integrated with Stanford's workgroup?

Not yet. You can freely assemble your project team from members in different groups.

## I accidentally deleted my repository, how do I restore it?

You are asked to confirm the deletion; If you still delete it, you maybe  able to find a checked-out repo on your computer, and
can use it to re-create your repo; If you or your co-workers don't have a local copy, then file a HelpSU ticket, we can
help you to get it from DR backups. The backup file is a repoistory.bundle package (actually a tar file). To restore from a bundle:
```
$ mkdir ~/mygit-bundle
$ cd  ~/mygit-bundle && tar xvf <path-to-bundle-file>
$ git clone ~/mygit-bundle <path-to-new-repo>
```

## How do I get notification for scheduled maintenance outage?

GitLab standing upgrade maintenance window is on Fridays from 9 p.m. to 11 p.m.  This schedule is outside of
UIT’s regular Thursday,  Saturday, and Sunday maintenance window so regular changes can use GitLab service for 
code releases or CI/CD jobs.

The actual scheduled upgrade, impact, and outage duration will be posted on https://code.stanford.edu and you also get notification on each git command line push if there is an active schedule.

If you have automated jobs that need to connect to the service, please put them outside of the standing maintenance window. It is also a good practice to add retires to improve application resilience.

## How do I migrate github project to GitLab?

Navigate to [https://code.stanford.edu/projects/new](https://code.stanford.edu/projects/new). At the top of the form, you will have the option to make either a blank project, to create a project from a template, or to import a project. Select to import a project. Next, select to import a project from GitHub.

Next, you need to let GitHub know it's okay for code.stanford.edu to look at all of your repos. You do this by giving code.stanford.edu a token (which works sort of like a password, but only for specific tasks and scopes). In a new browser tab, go to [https://github.com/settings/tokens](https://github.com/settings/tokens), and click "generate token". Under "token description" enter something like code.stanford.edu, and select the "repo" scope (which allows code.stanford.edu to read all of your repos, and do a few other things). In the next screen, copy your personal access token. This is the only time you'll be able to copy the token you just generated, though if you make a mistake, you can generate a new one at any time.

Drop your access token into the code.stanford.edu browser tab, and list your repos. For the repos you want to import, make sure that you have the correct namespace listed, and optionally change the repo name. Finally, click "import", and you should be done importing into GitLab.
